<?php
$good=0;

$db = new PDO('mysql:host=localhost;dbname=u21139', 'u21139', '2017773', array(PDO::ATTR_PERSISTENT => true));


if(empty($_GET["name"]) && empty($_GET["email"]) && empty($_GET["date"])){
    $name = $_COOKIE["name"];
    $email = $_COOKIE["email"];
    $date = $_COOKIE["date"];

    $error_check="";
    $error_date="";
    $error_email1="";
    $error_email2="";
    $error_name1="";
    $error_name2="";
    $check_gen1="";
    $check_gen2="checked";
    $check_lim = array("","","","checked");
    $text="";
    $answer="";
    $answer_style="";
    

}
else{
    if(!empty($_GET["name"])){
    $name = htmlentities($_GET["name"]);
    $res = $db->prepare("SELECT name FROM info WHERE name='$name'");
    $res->execute();
    $db_name = $res->fetch(PDO::FETCH_NUM);
    if(!$db_name){
        $num = strlen($name);
        $f=1;
        $error_name1 = "";
        $error_name2 = "";
        for($i = 0;$i<$num;$i++){
            if(($name[$i]<"A" || ($name[$i]>"Z" && $name[$i]<"a") || $name[$i]>"z"))$f=0;
        }
        if($f==0) {
            setcookie("error_name","error");
            $error_name1 = " style='color: red;' ";
            $error_name2 = "(a-z or A-Z)";
        }
        else{
            $error_name1 = " style='color: rgb(32, 179, 32);' ";
            $good++;
        }
    }
    else{
        setcookie("error_name","error");
        $error_name1 = " style='color: red;' ";
        $error_name2 = "the name is already taken";
    }
}
else {
    setcookie("error_name","error");
    $name = "";
    $error_name1 = " style='color: red;' ";
    $error_name2 = "enter a name";
}

if(!empty($_GET["email"])){
    $email = htmlentities($_GET["email"]);
    $num = strlen($email);
    $f=0;
    $error_email1="";
    $error_email2="";
    for($i=0;$i<$num;$i++){
        if($email[$i]=="@" && $i>0 && $i<$num-1)$f++;
    }
    if($f!=1){
        setcookie("error_email","error");
        $error_email1=" style='color: red;' ";
        $error_email2 = "( ___@___ )";
    }
    else{
        $error_email1 = " style='color: rgb(32, 179, 32);' ";
        $good++;
    }
}
else{
    setcookie("error_email","error");
    $email = "";
    $error_email1=" style='color: red;' ";
    $error_email2 = "( ___@___ )";
}

if(!empty($_GET["date"])){
    $date = htmlentities($_GET["date"]);
    $error_date = " style='color: rgb(32, 179, 32);' ";
    $good++;
}
else{
    setcookie("error_date","error");
    $date = "";
    $error_date = " style='color: red;' ";
}


if(!empty($_GET["radio1"])){
    $gen = $_GET["radio1"];
    if($gen=="m"){
        $check_gen1="checked";
        $check_gen2="";
    }
    else{
        $check_gen1="";
        $check_gen2="checked";
    }
}else{
    $check_gen1="checked";
    $check_gen2="";
}

if(!empty($_GET["radio2"])){
    $lim = $_GET["radio2"];
    $check_lim = array("","","","");
    for($i=0;$i<4;$i++){
        if($i==$lim)$check_lim[$i]="checked";
        else $check_lim[$i]="";
    }
}else{
    $check_lim = array("","","","checked");
}

if(!empty($_GET["textarea1"])){
    $text = $_GET["textarea1"];
}else{
    $text = "";
}

$sel="0";
if(isset($_GET["select"])){
    $sel=" ";
    foreach ($_GET["select"] as $valve){
    $sel .= $valve;
}
$sel = (int)$sel;
}

if(!empty($_GET["checkbox"])){
    $error_check = " style='color: rgb(32, 179, 32);' ";
    $good++;
}
else{
    $error_check=" style='color: red;' ";
}



if($good==4){
    $answer_style =" style='color: rgb(32, 179, 32);' ";
    $answer = "data sent";
    
    $stmt = $db->prepare("INSERT INTO info (name, email, date, gender, limbs, power_code, biography) VALUES (:f_name, :f_email, :f_date, :f_gen, :f_lim, :f_powers, :f_biograp)");
    $stmt->bindParam(':f_name', $name);
    $stmt->bindParam(':f_email', $email);
    $stmt->bindParam(':f_date', $date);
    $stmt->bindParam(':f_gen', $gen);
    $stmt->bindParam(':f_lim', $lim);
    $stmt->bindParam(':f_powers', $sel);
    $stmt->bindParam(':f_biograp', $text);
    $stmt->execute();

    setcookie("name", $name, time()+60*60*24*365);
    setcookie("email", $email, time()+60*60*24*365);
    setcookie("date", $date, time()+60*60*24*365);

    setcookie("error_name"," ",1);
    setcookie("error_email"," ",1);
    setcookie("error_date"," ",1);
}
else{
    $answer_style =" style='color: red;' ";
    $answer = "incorrectly filled in fields";
}
}

$result='<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="style.css">
    <title>MY site</title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script>
    </script>
</head>
<body>
    <div id="main">
        <div class="columns first_window" id="button1">Upload data</div>
        <div class="columns second_window" id="button2">Download data</div>
    </div>
    <div id="forms">
        <div class="columns_1 first_window" id="first_form">
            <div class="line"></div>
            <div>
                <form id="form1" method="GET" action="form.php">
                    <label' . $error_name1 . '>Name: 
                        <input  id="name" name="name" value=' . $name. '><br>'. $error_name2 .'
                    </label><br><br>
    
                    <label' . $error_email1 . '>E-mail:
                        <input  type="text" id="email" name="email" value=' . $email .'><br>'. $error_email2 .'
                    </label><br><br>

                    <lable' . $error_date . '>Date of birth:
                        <input type="date" name="date" value=' . $date . '><br><br>
                    </lable>
                    
                    Gender:
                    <label> Man
                        <input name="radio1" type="radio" value="m"'.$check_gen1.'>
                    </label>
                    <label> Woman
                        <input name="radio1" type="radio" value="w"'.$check_gen2.'>
                    </label><br><br>
    
                    Number of limbs:
                    <label><input name="radio2" type="radio" value="0"'.$check_lim[0].'> 1</label>
                    <label><input name="radio2" type="radio" value="1"'.$check_lim[1].'> 2</label>
                    <label><input name="radio2" type="radio" value="2"'.$check_lim[2].'> 3</label>
                    <label><input name="radio2" type="radio" value="3"'.$check_lim[3].'> 4</label><br><br>
                    
                    <label>Superpowers:<br>
                        <select name="select[]" size="3" multiple>
                            <option value="1">immortality</option>
                            <option value="2">passing through walls</option>
                            <option value="3">levitation</option>
                        </select>
                    </label><br><br>
                    
                    <label>Biography:<br>
                        <textarea cols="40" rows="3" name="textarea1">'.$text.'</textarea>
                    </label><br><br>
                    
                    <label ><div'. $error_check .'>I agree with the personal data processing policy:</div>
                        <input id="checkbox" name="checkbox" type="checkbox" value="ok">
                    </label><br><br>
                    
                    <input type="submit" value="Send"><br><br>
                    <div'. $answer_style .'>' . $answer . '</div>
                </form>
    
            </div>
        </div>
        <div class="columns_1 second_window" id="second_form">
            <div class="line"></div>
            <div>
                <form id="form2"  method="GET" action="form1.php">
                    <label >Name:  
                        <input  id="name" name="name">
                    </label><br><br>

                    <input type="submit" value="Receive">
                </form>
            </div>
        </div>
    </div>
</body>
</html>';

echo "$result";
?>
